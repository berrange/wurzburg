#
# Wurzburg Radio Telescope client
#
# Copyright (C) 2016 Daniel P. Berrange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import argparse
from datetime import date, datetime
import getpass
import logging
import os
import sys

from wurzburg import rpc
from wurzburg import config
from wurzburg import pager


class Command(object):

    def __init__(self, name, help):
        super(Command, self).__init__()
        self.name = name
        self.help = help
        self.pager = True

    def add_option(self, parser, config, *args, **kwargs):
        if args[0][0:1] == "-":
            if args[0][0:2] == "--":
                name = args[0][2:]
            else:
                name = args[1][2:]
        else:
            name = args[0]

        section = "command-" + self.name
        if config.has_option(section, name):
            defvalue = kwargs["default"]
            if type(defvalue) == list:
                kwargs["default"] = config.get_option_list(section, name)
            elif type(defvalue) == int:
                kwargs["default"] = config.get_option_int(section, name)
            elif type(defvalue) == bool:
                kwargs["default"] = config.get_option_bool(section, name)
            else:
                kwargs["default"] = config.get_option_string(section, name)

        return parser.add_argument(*args, **kwargs)

    def add_options(self, parser, config):
        pass

    def run(self, config, client, options):
        raise NotImplementedError("Subclass should override run method")

    def execute(self, config, client, options):
        if self.pager:
            pager.start_pager()
        try:
            self.run(config, client, options)
        finally:
            if self.pager:
                pager.stop_pager()


class CommandLogin(Command):

    def __init__(self):
        super(CommandLogin, self).__init__("login",
                                           "Login to the telescope")

    def add_options(self, parser, config):
        self.add_option(parser, config,
                        "--password-file", default=None,
                        help="Filename to read password from")
        self.add_option(parser, config,
                        "--password-fd", default=None,
                        help="File descriptor to read password from")
        self.add_option(parser, config,
                        "username",
                        help="User name")

    def _get_password(self, options):
        if options.password_fd is not None:
            with os.fdopen(int(options.password_fd), "r") as fh:
                return fh.read()[0:-1]
        elif options.password_file is not None:
            with open(options.password_file, "r") as fh:
                return fh.read()[0:-1]
        else:
            return getpass.getpass("Enter password: ")

    def run(self, config, client, options):
        password = self._get_password(options)
        try:
            client.login(options.username, password)
            print ("Login successful")
        except rpc.LoginFailed:
            print ("Login failed", file=sys.stderr)
            sys.exit(1)


class CommandSetLang(Command):

    LANG_MAP = {
        "en": rpc.Client.LANG_ENGLISH,
        "fr": rpc.Client.LANG_FRENCH,
    }

    def __init__(self):
        super(CommandSetLang, self).__init__("set-lang",
                                             "Set the API language")

    def add_options(self, parser, config):
        self.add_option(parser, config,
                        "language", default="en",
                        help="Language code, one of %s" % ",".join(
                            self.LANG_MAP.keys()))

    def run(self, config, client, options):
        lang = options.language

        if lang in self.LANG_MAP:
            client.set_language(self.LANG_MAP[lang])
            print ("Language changed to %s" % lang)
        else:
            print ("Unknown language code '%s', expecting one of %s" % ",".join(
                   self.LANG_MAP.keys()), file=sys.stderr)
            sys.exit(1)


class CommandSchedule(Command):

    def __init__(self):
        super(CommandSchedule, self).__init__("schedule",
                                              "View the day's schedule")

    def add_options(self, parser, config):
        today = date.today().strftime("%Y-%m-%d")
        self.add_option(parser, config,
                        "--date", default=today,
                        help="Date to view in format YYYY-MM-DD")

    def run(self, config, client, options):
        slots = client.get_schedule(options.date)

        LABELS = [ "Available", "Booked", "Reserved" ]
        for slot in slots:
            print ("Slot %d: %s-%s - %s" % (slot.index, slot.start, slot.end, LABELS[slot.status]))


class CommandReleaseSlot(Command):

    def __init__(self):
        super(CommandReleaseSlot, self).__init__("release-slot",
                                                 "Release a slot reservation")

    def add_options(self, parser, config):
        today = date.today().strftime("%Y-%m-%d")
        self.add_option(parser, config,
                        "--date", default=today,
                        help="Date to view in format YYYY-MM-DD")
        self.add_option(parser, config,
                        "slot", default=None, type=int,
                        help="Slot number to release")

    def run(self, config, client, options):
        options.slot = int(options.slot)
        if options.slot < 1 or options.slot > len(rpc.TimeSlot.TIME_MAP):
            print ("Slot number must be in range 1 -> %d" % len(rpc.TimeSlot.TIME_MAP))
            sys.exit(1)

        slots = client.get_schedule(options.date)

        slot = slots[options.slot - 1]

        if slot.status != rpc.TimeSlot.STATUS_YOURS:
            print ("Slot %d on %s is not reserved by you" % (options.slot, options.date))
            sys.exit(1)

        client.delete_reservation(slot)
        print ("Slot %d on %s is released" % (options.slot, options.date))


class CommandReserveSlot(Command):

    def __init__(self):
        super(CommandReserveSlot, self).__init__("reserve-slot",
                                                 "Acquire a slot reservation")

    def add_options(self, parser, config):
        today = date.today().strftime("%Y-%m-%d")
        self.add_option(parser, config,
                        "--date", default=today,
                        help="Date to view in format YYYY-MM-DD")
        self.add_option(parser, config,
                        "slot", default=None, type=int,
                        help="Slot number to acquire")
        self.add_option(parser, config,
                        "description", default="Observations",
                        nargs="?",
                        help="Description for reservation")

    def run(self, config, client, options):
        if options.slot < 1 or options.slot > len(rpc.TimeSlot.TIME_MAP):
            print ("Slot number must be in range 1 -> %d" % len(rpc.TimeSlot.TIME_MAP))
            sys.exit(1)

        slots = client.get_schedule(options.date)

        slot = slots[options.slot - 1]

        if slot.status == rpc.TimeSlot.STATUS_YOURS:
            print ("Slot %d on %s is already reserved by you" % (options.slot, options.date))
            sys.exit(1)
        elif slot.status == rpc.TimeSlot.STATUS_BOOKED:
            print ("Slot %d on %s is already reserved another user" % (options.slot, options.date))
            sys.exit(1)

        client.create_reservation(slot, options.description)
        print ("Slot %d on %s is acquired" % (options.slot, options.date))


class CommandReservations(Command):

    def __init__(self):
        super(CommandReservations, self).__init__("reservations",
                                                  "List your reservations")

    def add_options(self, parser, config):
        today = date.today().strftime("%Y-%m-%d")
        self.add_option(parser, config,
                        "--since", default=today,
                        help="Filter to reservations since YYYY-MM-DD")

    def run(self, config, client, options):
        reservations = client.get_reservations()

        since = datetime.strptime(options.since, "%Y-%m-%d")

        for res in reservations:
            then = datetime.strptime(res.slot.date, "%Y-%m-%d")
            if then >= since:
                print ("Reservation ID %s for %s slot %d: %s-%s: %s" % (
                    res.id, res.slot.date, res.slot.index,
                    res.slot.start, res.slot.end,
                    res.description))


class CommandIsReserved(Command):

    def __init__(self):
        super(CommandIsReserved, self).__init__("is-reserved",
                                                "Check if you have a current reservation")

    def run(self, config, client, options):
        if client.is_reserved():
            print ("The telescope is currently reserved by you")
        else:
            print ("The telescope is not currently reserved by you")
            sys.exit(1)


class CommandIsAntennaOnline(Command):

    def __init__(self):
        super(CommandIsAntennaOnline, self).__init__("is-antenna-online",
                                                     "Check if the antenna is online for observing")

    def run(self, config, client, options):
        if client.is_antenna_online():
            print ("The telescope antenna is online for observing")
        else:
            print ("The telescope antenna is offline for maintenance")
            sys.exit(1)


class CommandTool(object):

    def __init__(self):
        self.commands = {}

    def add_options(self, parser):
        parser.add_argument("-c", "--config", default="~/.wurzburg.conf",
                            help=("Override config file (default %s)" %
                                  "~/.wurzburg.conf"))
        parser.add_argument("-d", "--debug",
                            help="Display debugging information",
                            action="store_true")
        parser.add_argument("-q", "--quiet",
                            help="Supress display of warnings",
                            action="store_true")

    def add_command(self, subparser, config, cmdclass, name=None, help=None):
        cmd = cmdclass()
        if name is not None:
            cmd.name = name
        if help is not None:
            cmd.help = help

        parser = subparser.add_parser(cmd.name, help=cmd.help)
        cmd.add_options(parser, config)
        parser.set_defaults(func=cmd.execute)
        self.commands[cmd.name] = cmd

    def add_default_commands(self, subparser, config):
        self.add_command(subparser, config, CommandLogin)
        self.add_command(subparser, config, CommandSetLang)
        self.add_command(subparser, config, CommandSchedule)
        self.add_command(subparser, config, CommandReserveSlot)
        self.add_command(subparser, config, CommandReleaseSlot)
        self.add_command(subparser, config, CommandReservations)
        self.add_command(subparser, config, CommandIsReserved)
        self.add_command(subparser, config, CommandIsAntennaOnline)

    def add_config_commands(self, subparser, config):
        aliases = config.get_command_aliases()
        for alias in aliases:
            basecmd = config.get_command_alias_basecmd(alias)
            help = config.get_command_alias_help(alias)

            if basecmd not in self.commands:
                raise Exception("Unknown base command '%s'" % basecmd)

            klass = type(self.commands[basecmd])
            self.add_command(subparser, config, klass, alias, help)

    def execute(self, argv):
        miniparser = argparse.ArgumentParser(add_help=False)
        miniparser.add_argument("-c", "--config", default="~/.wurzburg.conf",
                                help=("Override config file (default %s)" %
                                      "~/.wurzburg.conf"))
        options, remaining = miniparser.parse_known_args(argv)

        cfg = config.ConfigFile(options.config)
        client = rpc.Client(cfg.get_server_hostname(),
                            cfg.get_server_port(),
                            cfg.get_server_cookiejar())

        parser = argparse.ArgumentParser(description="Gerrymander client")
        self.add_options(parser)
        subparser = parser.add_subparsers()
        subparser.required = True
        subparser.dest = "command"
        self.add_default_commands(subparser, cfg)
        self.add_config_commands(subparser, cfg)

        options = parser.parse_args(argv)


        level = logging.WARNING
        if options.debug:
            level = logging.DEBUG
        elif options.quiet:
            level = logging.ERROR

        logging.basicConfig(level=level,
                            format='%(asctime)s %(levelname)s: %(message)s',
                            stream=sys.stderr)

        options.func(cfg, client, options)
