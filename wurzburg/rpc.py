#
# Wurzburg Radio Telescope client
#
# Copyright (C) 2016 Daniel P. Berrange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import requests
from hashlib import md5


class LoginFailed(Exception):
    pass


class DeleteReservationFailed(Exception):
    pass


class CreateReservationFailed(Exception):
    pass


class GenerateZIPFileFailed(Exception):
    pass

class TimeSlot(object):

    STATUS_AVAILABLE = 0
    STATUS_BOOKED = 1
    STATUS_YOURS = 2

    TIME_MAP = [
        ["00:00:00", "04:00:00"],
        ["04:00:00", "08:00:00"],
        ["08:00:00", "12:00:00"],
        ["12:00:00", "16:00:00"],
        ["16:00:00", "20:00:00"],
        ["20:00:00", "23:59:59"],
    ]


    def __init__(self, index,
                 date, start, end, status):

        self.index = index
        self.date = date
        self.start = start
        self.end = end
        self.status = status

    def __repr__(self):
        status = ["Available", "Booked", "Yours"]
        return "%s (%s) %s-%s %s" % (self.date, self.index, self.start, self.end, self.status)


class Reservation(object):

    def __init__(self,
                 id, slot, deletable, pending,
                 user_id, user_email, user_forename, user_surname,
                 description):

        self.id = id
        self.slot = slot
        self.deletable = deletable
        self.pending = pending

        self.user_id = user_id
        self.user_email = user_email
        self.user_forename = user_forename
        self.user_surname = user_surname

        self.description = description

    def __repr__(self):
        return "%s at %s %s-%s for %s %s (%s) <%s> %s" % (
            self.id, self.slot.date, self.slot.start, self.slot.end,
            self.user_forename, self.user_surname, self.user_id, self.user_email,
            self.description)


class Client(object):

    def __init__(self, hostname, port, cookiejar):
        self.hostname = hostname
        self.port = port
        self.cookiejar = cookiejar
        self.cookies = {}

        self._load_cookies()

    LANG_ENGLISH = 1
    LANG_FRENCH = 2

    def _load_cookies(self):
        try:
            with open(self.cookiejar, "r") as fh:
                self.cookies = json.load(fh)
        except:
            pass

    def _save_cookies(self):
        with open(self.cookiejar, "w") as fh:
            json.dump(self.cookies, fh)

    def _request(self, path, params):
        url = "http://"  + self.hostname
        if self.port != 80:
            url = "%s:%d" % (self.hostname, self.port)

        url = url + path

        resp = requests.post(url, data=params, cookies=self.cookies)

        if ("PHPSESSID" in resp.cookies and
            ("PHPSESSID" not in self.cookies or
             resp.cookies["PHPSESSID"] != self.cookies["PHPSESSID"])):
            self.cookies["PHPSESSID"] = resp.cookies["PHPSESSID"]
            self._save_cookies()

        return resp

    def set_language(self, lang):
        resp = self._request("/functions/php_functions/chLang.php",
                             {
                                 "id": lang
                             })

    def login(self, username, password):
        h = md5()
        h.update(password.encode("utf-8"))

        resp = self._request("/functions/php_functions/connect.php",
                             {
                                 "connexion": "",
                                 "login": username,
                                 "mdp": h.hexdigest()
                             })

        params = resp.json()
        if not params["success"]:
            raise LoginFailed(params["message"])

    def get_schedule(self, date):

        resp = self._request("/functions/php_functions/sqlRequest.php",
                             {
                                 "action": "checkRes",
                                 "date": date,
                             })

        data = resp.json()
        slots = data["reserv"]
        slotlist = []

        for i in range(len(TimeSlot.TIME_MAP)):
            slot = TimeSlot(i + 1, date,
                            TimeSlot.TIME_MAP[i][0],
                            TimeSlot.TIME_MAP[i][1], int(slots[i]))
            slotlist.append(slot)
        return slotlist

    def is_reserved(self):
        resp = self._request("/functions/php_functions/sqlRequest.php",
                             {
                                 "action": "isRes",
                             })

        data = resp.json()
        return data["isRes"] == 1

    def get_reservations(self):

        resp = self._request("/functions/php_functions/sqlRequest.php",
                             {
                                 "action": "listUsRes",
                                 "page": 1,
                                 "start": 0,
                                 "limit": 1
                             })

        data = resp.json()
        reslist = []

        for row in data:
            if row["res_id"] == "-":
                continue

            res = Reservation(
                id = row["res_id"],

                slot = TimeSlot(
                    index = int(row["cren_id"]),
                    date = row["res_date"],
                    start = row["res_hstart"],
                    end = row["res_hend"],
                    status = TimeSlot.STATUS_YOURS
                ),

                pending = row["res_date_ok"],
                deletable = row["res_date_del"],

                user_id = row["user_id"],
                user_forename = row["user_name"],
                user_surname = row["user_surname"],
                user_email = row["user_mail"],

                description = row["description"])

            reslist.append(res)
        return reslist

    def delete_reservation(self, slot):
        print "Hello"
        resp = self._request("/functions/php_functions/sqlRequest.php",
                             {
                                 "action": "delRes",
                                 "date": slot.date,
                                 "cren": slot.index,
                             })

        data = resp.json()
        print "Hello"
        print str(data)
        if not data["success"]:
            raise DeleteReservationFailed()

    def create_reservation(self, slot, description):
        resp = self._request("/functions/php_functions/sqlRequest.php",
                             {
                                 "action": "addRes",
                                 "date": slot.date,
                                 "cren": slot.index,
                                 "hStart": slot.start,
                                 "hEnd": slot.end,
                                 "desc": description
                             })

        data = resp.json()

        if not data["success"]:
            raise CreateReservationFailed()

    def is_antenna_online(self):
        resp = self._request("/functions/php_functions/chAntStat.php",
                             {
                                 "action": "checkAntStat",
                             })

        data = resp.json()
        return int(data["status"]) != 0

    def generate_result_zip(self, reservation):
        resp = self._request("/functions/php_functions/zipFiles.php",
                             {
                                 "action": "zipRes",
                                 "date": reservation.slot.date,
                                 "res": reservation.id,
                                 "user": reservation.user_id,
                             })

        data = resp.json()

        if not data["success"]:
            raise GenerateZIPFileFailed()

        return data["message"]
