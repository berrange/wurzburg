#
# Wurzburg Radio Telescope client
#
# Copyright (C) 2016 Daniel P. Berrange
# Copyright (C) 2015 Red Hat
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess
import sys

pagerproc = None

def get_pager_env(name):
    if name not in os.environ:
        return None
    pager = os.environ[name]
    return pager

def get_pager():
    if not sys.stdout.isatty():
        return None

    pager = get_pager_env("WURZBURG_PAGER")
    if not pager:
        pager = get_pager_env("PAGER")
    if not pager:
        pager = "less"

    if pager == "cat":
        return None

    return pager

def stop_pager():
    global pagerproc
    if pagerproc is None:
        return

    sys.stdout.flush()
    sys.stderr.flush()
    os.close(1)
    pagerproc.stdin.close()
    pagerproc.wait()
    pagerproc = None

def start_pager():
    if not sys.stdout.isatty():
        return

    pager = get_pager()
    if not pager:
        return

    if "LESS" not in os.environ:
        os.environ["LESS"] = "FRSX"

    oldstdout = os.dup(1)
    global pagerproc
    pagerproc = subprocess.Popen([pager],
                                 stdin=subprocess.PIPE,
                                 stdout=oldstdout,
                                 close_fds=True)

    os.close(oldstdout)
    os.dup2(pagerproc.stdin.fileno(), 1)
