#
# Wurzburg Radio Telescope client
#
# Copyright (C) 2016 Daniel P. Berrange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path

try:
    import configparser
except:
    import ConfigParser as configparser

class ConfigFile(object):

    def __init__(self, filename):
        self.filename = os.path.expanduser(filename)
        self.config = configparser.ConfigParser()
        self.config.read([self.filename])

    def has_option(self, section, name):
        return self.config.has_option(section, name)

    def get_option_string(self, section, name, defvalue=None):
        if not self.config.has_option(section, name):
            return defvalue
        return self.config.get(section, name)

    def get_option_int(self, section, name, defvalue=None):
        if not self.config.has_option(section, name):
            return defvalue
        return int(self.config.get(section, name))

    def get_option_list(self, section, name, defvalue=None):
        if not self.config.has_option(section, name):
            return defvalue
        value = self.config.get(section, name)
        return list(map(lambda x: x.strip(), value.split(",")))

    def get_option_bool(self, section, name, defvalue=None):
        if not self.config.has_option(section, name):
            return defvalue
        value = self.config.get(section, name).strip()
        if value.lower() in ["true", "yes"]:
            return True
        return False

    def get_server_hostname(self):
        return self.get_option_string(
            "server", "hostname",
            "serveurwurzburg.obs.u-bordeaux1.fr")

    def get_server_port(self):
        return self.get_option_int(
            "server", "port", 80)

    def get_server_cookiejar(self):
        return os.path.expanduser(
            self.get_option_string(
                "server", "cookiejar", "~/.wurzburg.cookies"))

    def get_command_aliases(self):
        if not self.config.has_option("commands", "aliases"):
            return []
        value = self.config.get("commands", "aliases")
        return list(map(lambda x: x.strip(), value.split(",")))

    def get_command_alias_basecmd(self, aliasname):
        section = "alias-" + aliasname
        return self.config.get(section, "basecmd")

    def get_command_alias_help(self, aliasname):
        section = "alias-" + aliasname
        return self.config.get(section, "help")
